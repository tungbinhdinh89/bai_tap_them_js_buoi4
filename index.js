// Bài 1
function yesterday() {
  var day = document.getElementById('day').value * 1;
  var month = document.getElementById('month').value * 1;
  var year = document.getElementById('year').value * 1;
  if (
    day > 31 ||
    month > 12 ||
    (month === 2 &&
      day >= 29 &&
      (year % 400 !== 0 || (year % 4 !== 0 && year % 100 === 0)))
  ) {
    document.getElementById(
      'result'
    ).innerText = `Ngày hoặc tháng không hợp lệ`;
  } else if (
    day === 1 &&
    (month === 5 || month === 7 || month === 10 || month === 12)
  ) {
    document.getElementById('result').innerText = `30/${month - 1}/${year}`;
  } else if (day === 1 && month === 1) {
    document.getElementById('result').innerText = `31/${12}/${year - 1}`;
  } else if (
    day === 1 &&
    (month === 2 ||
      month === 4 ||
      month === 6 ||
      month === 8 ||
      month === 9 ||
      month === 11)
  ) {
    document.getElementById('result').innerText = `31/${month - 1}/${year}`;
  } else if (
    day === 1 &&
    month === 3 &&
    ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0)
  ) {
    document.getElementById('result').innerText = `29/${month - 1}/${year}`;
  } else if (day === 1 && month === 3) {
    document.getElementById('result').innerText = `28/${month - 1}/${year}`;
  } else {
    document.getElementById('result').innerText = `${day - 1}/${month}/${year}`;
  }
}

function tomorrow() {
  var day = document.getElementById('day').value * 1;
  var month = document.getElementById('month').value * 1;
  var year = document.getElementById('year').value * 1;
  if (
    day > 31 ||
    month > 12 ||
    (month === 2 &&
      day >= 29 &&
      (year % 400 !== 0 || (year % 4 !== 0 && year % 100 === 0)))
  ) {
    document.getElementById(
      'result'
    ).innerText = `Ngày hoặc tháng không hợp lệ`;
  } else if (
    day === 31 &&
    (month === 1 ||
      month === 3 ||
      month === 5 ||
      month === 7 ||
      month === 8 ||
      month === 10)
  ) {
    document.getElementById('result').innerText = `1/${month + 1}/${year}`;
  } else if (day === 31 && month === 12) {
    document.getElementById('result').innerText = `1/${1}/${year + 1}`;
  } else if (
    day === 31 &&
    (month === 4 || month === 6 || month === 8 || month === 9 || month === 11)
  ) {
    document.getElementById('result').innerText = `1/${month + 1}/${year}`;
  } else if (
    (day === 29 && month === 2 && year % 4 === 0 && year % 100 !== 0) ||
    year % 400 === 0
  ) {
    document.getElementById('result').innerText = `1/${month + 1}/${year}`;
  } else if (day === 28 && month === 2) {
    document.getElementById('result').innerText = `1/${month + 1}/${year}`;
  } else {
    document.getElementById('result').innerText = `${day + 1}/${month}/${year}`;
  }
}

// Bài 2

function findDate() {
  var month2 = document.getElementById('month2').value * 1;
  var year2 = document.getElementById('year2').value * 1;
  if (month2 > 12 || year2 < 1920) {
    document.getElementById(
      'result2'
    ).innerText = `Tháng phải nhỏ hơn 12 và năm phải lớn hơn 1920`;
  } else if (
    month2 === 1 ||
    month2 === 3 ||
    month2 === 5 ||
    month2 === 7 ||
    month2 === 8 ||
    month2 === 10 ||
    month2 === 12
  ) {
    document.getElementById(
      'result2'
    ).innerText = `Tháng ${month2} năm ${year2} thì có 31 ngày`;
  } else if (
    month2 === 2 &&
    ((year2 % 4 === 0 && year2 % 100 !== 0) || year2 % 400 === 0)
  ) {
    document.getElementById(
      'result2'
    ).innerText = `Tháng ${month2} năm ${year2} thì có 29 ngày`;
  } else if (month2 === 2) {
    document.getElementById(
      'result2'
    ).innerText = `Tháng ${month2} năm ${year2} thì có 28 ngày`;
  } else {
    document.getElementById(
      'result2'
    ).innerText = `Tháng ${month2} năm ${year2} thì có 30 ngày`;
  }
}

// Bài 03
function readNumber() {
  var getNumber = document.getElementById('number').value * 1;
  var getHundreds = Math.floor(getNumber / 100);
  var getDozens = Math.floor((getNumber % 100) / 10);
  var getUnits = getNumber % 10;
  //  read hundreads
  if (getHundreds === 1) {
    getHundreds = 'Một Trăm';
  } else if (getHundreds === 2) {
    getHundreds = 'Hai Trăm';
  } else if (getHundreds === 3) {
    getHundreds = 'Ba Trăm';
  } else if (getHundreds === 4) {
    getHundreds = 'Bốn Trăm';
  } else if (getHundreds === 5) {
    getHundreds = 'Năm Trăm';
  } else if (getHundreds === 6) {
    getHundreds = 'Sáu Trăm';
  } else if (getHundreds === 7) {
    getHundreds = 'Bảy Trăm';
  } else if (getHundreds === 8) {
    getHundreds = 'Tám Trăm';
  } else if (getHundreds === 9) {
    getHundreds = 'Chín Trăm';
  }
  //  read dozens
  if (getDozens === 1) {
    getDozens = 'Mười';
  } else if (getDozens === 2) {
    getDozens = 'Hai Mươi';
  } else if (getDozens === 3) {
    getDozens = 'Ba Mươi';
  } else if (getDozens === 4) {
    getDozens = 'Bốn Mươi';
  } else if (getDozens === 5) {
    getDozens = 'Năm Mươi';
  } else if (getDozens === 6) {
    getDozens = 'Sáu Mươi';
  } else if (getDozens === 7) {
    getDozens = 'Bảy Mươi';
  } else if (getDozens === 8) {
    getDozens = 'Tám Mươi';
  } else if (getDozens === 9) {
    getDozens = 'Chín Mươi';
  }
  // read units
  if (getUnits === 1) {
    getUnits = 'Một ';
  } else if (getUnits === 2) {
    getUnits = 'Hai ';
  } else if (getUnits === 3) {
    getUnits = 'Ba ';
  } else if (getUnits === 4) {
    getUnits = 'Bốn ';
  } else if (getUnits === 5) {
    getUnits = 'Năm ';
  } else if (getUnits === 6) {
    getUnits = 'Sáu ';
  } else if (getUnits === 7) {
    getUnits = 'Bảy ';
  } else if (getUnits === 8) {
    getUnits = 'Tám ';
  } else if (getUnits === 9) {
    getUnits = 'Chín ';
  }

  document.getElementById(
    'result3'
  ).innerText = `${getHundreds} ${getDozens} ${getUnits}`;
}
// Bài 04

function farestFromSchool() {
  // Student
  var studentName1 = document.getElementById('name1').value;
  var student1X = document.getElementById('pointX1').value * 1;
  var student1Y = document.getElementById('pointY1').value * 1;

  var studentName2 = document.getElementById('name2').value;
  var student2X = document.getElementById('pointX2').value * 1;
  var student2Y = document.getElementById('pointY2').value * 1;

  var studentName3 = document.getElementById('name3').value;
  var student3X = document.getElementById('pointX3').value * 1;
  var student3Y = document.getElementById('pointY3').value * 1;
  // School
  var schoolX = document.getElementById('pointXT').value * 1;
  var schoolY = document.getElementById('pointYT').value * 1;

  // distance
  var studentDistance1 = Math.sqrt(
    (schoolX - student1X) * (schoolX - student1X) +
      (schoolY - student1Y) * (schoolY - student1Y)
  );
  var studentDistance2 = Math.sqrt(
    (schoolX - student2X) * (schoolX - student2X) +
      (schoolY - student2Y) * (schoolY - student2Y)
  );
  var studentDistance3 = Math.sqrt(
    (schoolX - student3X) * (schoolX - student3X) +
      (schoolY - student3Y) * (schoolY - student3Y)
  );

  console.log(studentDistance1);
  console.log(studentDistance2);
  console.log(studentDistance3);
  console.log(studentName1);
  console.log(studentName2);
  console.log(studentName3);
  // which student is farthest from school
  if (
    (studentDistance1 > studentDistance2 &&
      studentDistance2 > studentDistance3) ||
    (studentDistance1 > studentDistance3 && studentDistance3 > studentDistance2)
  ) {
    document.getElementById('result4').innerText = `${studentName1}`;
  } else if (
    (studentDistance2 > studentDistance1 &&
      studentDistance1 > studentDistance3) ||
    (studentDistance2 > studentDistance3 && studentDistance3 > studentDistance1)
  ) {
    document.getElementById('result4').innerText = `${studentName2}`;
  } else if (
    (studentDistance3 > studentDistance2 &&
      studentDistance2 > studentDistance1) ||
    (studentDistance3 > studentDistance1 && studentDistance1 > studentDistance2)
  ) {
    document.getElementById('result4').innerText = `${studentName3}`;
  }
}
